import { Component } from '@angular/core';

@Component({
  selector: 'app-root',//indexa el html del componente
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'automattizator';
}
