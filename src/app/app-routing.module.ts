import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './vistas/login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login',component: LoginComponent },
  { path: 'visibilidadGPS', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

